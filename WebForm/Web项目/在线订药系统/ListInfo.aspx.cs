﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 在线订药系统
{
    public partial class ListInfo : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string GetState(string state)
        {
            if (state=="1")
            {
                return "未配送";
            }else if (state=="2")
            {
                return "已配送";
            }else if (state == "3")
            {
                return "配送中";
            }
            else
            {
                return "未知";
            }
        }

        protected void Button1_Click(object sender, EventArgs e)
        {
            string sql = $"select * from orderinfo inner join userinfo on orderinfo.userid=userinfo.userid  where MedicineName like '%{txtCondition.Text.Trim()}%'";
            SqlDataSource1.SelectCommand= sql;  
        }
    }
}