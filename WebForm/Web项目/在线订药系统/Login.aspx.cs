﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace 在线订药系统
{
    public partial class Login : System.Web.UI.Page
    {
        /// <summary>
        /// 页面加载事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Page_Load(object sender, EventArgs e)
        {
            //非介入式验证设置为None
            UnobtrusiveValidationMode = UnobtrusiveValidationMode.None;
        }

        
        /// <summary>
        /// 登录的单击事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void Button1_Click(object sender, EventArgs e)
        {
            try
            {
                //获取用户名和密码作为查询条件构建查询语句
                string sql = $"select * from Userinfo " +
                    $"where username='{txtName.Text.Trim()}' and password='{txtPwd.Text.Trim()}'";

                //数据库连接信息
                string constr = "server=.;uid=sa;pwd=123456;database=OrderMedicineDB";

                //数据库适配器
                SqlDataAdapter adapter = new SqlDataAdapter(sql, constr);

                //数据表
                DataTable dt = new DataTable();

                //Fill执行sql语句，结果填充到dt对象
                adapter.Fill(dt);

                //dt.Rows.Count :查询结果集行数是否大于0，大于0查询到用户，等于没有此用户
                if (dt.Rows.Count > 0)
                {

                    //查询到用户，登录成功
                    //将用户的信息保存到Session对象中，用户级别的数据
                    //Session[变量名] = 值;
                    //用户Id 和用户名保存到Session对象
                    Session["userId"] = dt.Rows[0]["UserId"];
                    Session["userName"] = dt.Rows[0]["UserName"];

                    //保存登录时间
                    Session["loginTime"] = DateTime.Now;

                    //Redirect:重新定位到新的页面，参数目标页面的url
                    Response.Redirect("~/ListInfo.aspx");

                }
                else
                {
                    //登录失败
                    Response.Write("<script>alert('用户名和密码错误')</script>");

                }
            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
           


        }
    }
}