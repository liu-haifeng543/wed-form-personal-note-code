﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="在线订药系统.Register" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
             <asp:Label ID="Label1" runat="server" Text="用户名"></asp:Label>
            <asp:TextBox ID="txtName" runat="server" ></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtName" Text="*"  ErrorMessage="用户名不能为空" ForeColor="Red"></asp:RequiredFieldValidator>
            <br />
            <asp:Label ID="Label2" runat="server" Text="密码"></asp:Label>
            <asp:TextBox ID="txtPwd" runat="server" TextMode="Password"></asp:TextBox>
            <br />
            <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtPwd" Text="*" ErrorMessage="密码不能为空" ForeColor="Red"></asp:RequiredFieldValidator>
            
            <br />

            <asp:Label ID="Label3" runat="server" Text="确认密码"></asp:Label>
            <asp:TextBox ID="txtPwd2" runat="server" TextMode="Password"></asp:TextBox>
            <%-- CompareValidator:与另外一个控件进行值的比较
                属性：ControlToValidate：指明要验证的控件ID
                      ControlToCompare:指明要比较的另一个控件ID
                --%>
             <br />
            <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtPwd2" ControlToCompare="txtPwd" ErrorMessage="两次密码不一致"></asp:CompareValidator>
            <br />
            <asp:Button ID="Button1" runat="server" Text="注册"  />
            <%-- PostBackUrl:跳转到指定页面，以Post提交 --%>
            <asp:Button ID="Button2" runat="server" Text="重置" CausesValidation="false"  />
            

        </div>
    </form>
</body>
</html>
