﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ListInfo.aspx.cs" Inherits="在线订药系统.ListInfo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            药名：<asp:TextBox ID="txtCondition" runat="server"></asp:TextBox><asp:Button ID="Button1" runat="server" Text="查询" OnClick="Button1_Click" />
        </div>
        <div>
            <asp:GridView ID="GridView1" runat="server" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1" AllowPaging="True" AllowSorting="True" PageSize="2">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id" HeaderText="编号" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="MedicineName" HeaderText="药名" SortExpression="MedicineName" />
                    <asp:BoundField DataField="UserName" HeaderText="下单人姓名" SortExpression="UserName" />
                    <asp:BoundField DataField="AddTime" HeaderText="下单时间" SortExpression="AddTime" />
                    <asp:BoundField DataField="RealName" HeaderText="收货人姓名" SortExpression="RealName" />
                    <asp:BoundField DataField="Mobile" HeaderText="收货电话" SortExpression="Mobile" />
                    <asp:BoundField DataField="Address" HeaderText="收货地址" SortExpression="Address" />
                    <asp:TemplateField HeaderText="状态" SortExpression="State">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("State") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <%-- <%# %> 进行后台代码的调用进行数据绑定
                                Bind():双向绑定函数，与数据库字段进行双向绑定，可以读，可以回写
                                Eval():单向绑定函数，只能读取数据，不能回写
                           --%>
                            <%--<asp:Label ID="Label1" runat="server" Text='<%# Eval("State").ToString()=="1"?"未配送":"已配送" %>'></asp:Label>--%>
                            <asp:Label ID="Label1" runat="server" Text='<%# GetState(Eval("State").ToString()) %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SendTime" HeaderText="配送时间" SortExpression="SendTime" />
                    <asp:TemplateField HeaderText="配送类型" SortExpression="SendType">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("SendType") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Eval("SendType").ToString()=="False"?"商家配送":"自提" %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Remark" HeaderText="备注" SortExpression="Remark" />
                </Columns>
            </asp:GridView>
            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OrderMedicineDBConnectionString %>" 
                DeleteCommand="DELETE FROM [OrderInfo] WHERE [Id] = @Id" 
                InsertCommand="INSERT INTO [OrderInfo] ([MedicineName], [UserId], [AddTime], [RealName], [Mobile], [Address], [State], [SendTime], [SendType], [Remark]) VALUES (@MedicineName, @UserId, @AddTime, @RealName, @Mobile, @Address, @State, @SendTime, @SendType, @Remark)" 
                SelectCommand="SELECT o.Id, o.MedicineName, o.UserId, o.AddTime, o.RealName, o.Mobile, o.Address, o.State, o.SendTime, o.SendType, o.Remark, u.UserId AS Expr1, u.UserName, u.Password FROM OrderInfo AS o INNER JOIN UserInfo AS u ON o.UserId = u.UserId" 
                UpdateCommand="UPDATE [OrderInfo] SET [MedicineName] = @MedicineName, [AddTime] = @AddTime, [RealName] = @RealName, [Mobile] = @Mobile, [Address] = @Address, [State] = @State, [SendTime] = @SendTime, [SendType] = @SendType, [Remark] = @Remark WHERE [Id] = @Id">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="MedicineName" Type="String" />
                    <%--<asp:Parameter Name="UserId" Type="Int32" />--%>
                    <asp:Parameter Name="AddTime" Type="DateTime" />
                    <asp:Parameter Name="RealName" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="State" Type="Int32" />
                    <asp:Parameter Name="SendTime" Type="DateTime" />
                    <asp:Parameter Name="SendType" Type="Boolean" />
                    <asp:Parameter Name="Remark" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="MedicineName" Type="String" />
                    <asp:Parameter Name="UserId" Type="Int32" />
                    <asp:Parameter Name="AddTime" Type="DateTime" />
                    <asp:Parameter Name="RealName" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="State" Type="Int32" />
                    <asp:Parameter Name="SendTime" Type="DateTime" />
                    <asp:Parameter Name="SendType" Type="Boolean" />
                    <asp:Parameter Name="Remark" Type="String" />
                    <asp:Parameter Name="Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>
        </div>
    </form>
    <p>
&nbsp;
    </p>
</body>
</html>
