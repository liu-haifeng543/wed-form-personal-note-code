﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="WebForm1.aspx.cs" Inherits="在线订药系统.WebForm1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" DataKeyNames="Id" DataSourceID="SqlDataSource1">
                <Columns>
                    <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
                    <asp:BoundField DataField="Id" HeaderText="编号" InsertVisible="False" ReadOnly="True" SortExpression="Id" />
                    <asp:BoundField DataField="MedicineName" HeaderText="药名" SortExpression="MedicineName" />
                    <asp:TemplateField HeaderText="下单人" SortExpression="UserName">
                        <EditItemTemplate>
                            <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("UserName") %>'></asp:TextBox>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label2" runat="server" Text='<%# Bind("UserName") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="AddTime" HeaderText="AddTime" SortExpression="AddTime" />
                    <asp:BoundField DataField="RealName" HeaderText="RealName" SortExpression="RealName" />
                    <asp:BoundField DataField="Mobile" HeaderText="Mobile" SortExpression="Mobile" />
                    <asp:BoundField DataField="Address" HeaderText="Address" SortExpression="Address" />
                    <asp:TemplateField HeaderText="状态" SortExpression="State">
                        <EditItemTemplate>
                            <asp:RadioButtonList ID="RadioButtonList1" runat="server">
                            </asp:RadioButtonList>
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:Label ID="Label3" runat="server" Text='<%# Eval("State").ToString()=="1"?"未配送":"已配送" %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="SendTime" HeaderText="SendTime" SortExpression="SendTime" />
                    <asp:TemplateField HeaderText="配送方式" SortExpression="SendType">
                        <EditItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("SendType") %>' />
                        </EditItemTemplate>
                        <ItemTemplate>
                            <asp:CheckBox ID="CheckBox1" runat="server" Checked='<%# Bind("SendType") %>' Enabled="false" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Remark" HeaderText="Remark" SortExpression="Remark" />
                </Columns>
            </asp:GridView>

            <asp:SqlDataSource ID="SqlDataSource1" runat="server" ConnectionString="<%$ ConnectionStrings:OrderMedicineDBConnectionString2 %>" DeleteCommand="DELETE FROM [OrderInfo] WHERE [Id] = @Id" InsertCommand="INSERT INTO [OrderInfo] ([MedicineName], [UserId], [AddTime], [RealName], [Mobile], [Address], [State], [SendTime], [SendType], [Remark]) VALUES (@MedicineName, @UserId, @AddTime, @RealName, @Mobile, @Address, @State, @SendTime, @SendType, @Remark)" SelectCommand="SELECT o.Id, o.MedicineName, o.UserId, o.AddTime, o.RealName, o.Mobile, o.Address, o.State, o.SendTime, o.SendType, o.Remark, u.UserName  FROM OrderInfo AS o INNER JOIN UserInfo AS u ON o.UserId = u.UserId" UpdateCommand="UPDATE [OrderInfo] SET [MedicineName] = @MedicineName, [UserId] = @UserId, [AddTime] = @AddTime, [RealName] = @RealName, [Mobile] = @Mobile, [Address] = @Address, [State] = @State, [SendTime] = @SendTime, [SendType] = @SendType, [Remark] = @Remark WHERE [Id] = @Id">
                <DeleteParameters>
                    <asp:Parameter Name="Id" Type="Int32" />
                </DeleteParameters>
                <InsertParameters>
                    <asp:Parameter Name="MedicineName" Type="String" />
                    <asp:Parameter Name="UserId" Type="Int32" />
                    <asp:Parameter Name="AddTime" Type="DateTime" />
                    <asp:Parameter Name="RealName" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="State" Type="Int32" />
                    <asp:Parameter Name="SendTime" Type="DateTime" />
                    <asp:Parameter Name="SendType" Type="Boolean" />
                    <asp:Parameter Name="Remark" Type="String" />
                </InsertParameters>
                <UpdateParameters>
                    <asp:Parameter Name="MedicineName" Type="String" />
                    <asp:Parameter Name="UserId" Type="Int32" />
                    <asp:Parameter Name="AddTime" Type="DateTime" />
                    <asp:Parameter Name="RealName" Type="String" />
                    <asp:Parameter Name="Mobile" Type="String" />
                    <asp:Parameter Name="Address" Type="String" />
                    <asp:Parameter Name="State" Type="Int32" />
                    <asp:Parameter Name="SendTime" Type="DateTime" />
                    <asp:Parameter Name="SendType" Type="Boolean" />
                    <asp:Parameter Name="Remark" Type="String" />
                    <asp:Parameter Name="Id" Type="Int32" />
                </UpdateParameters>
            </asp:SqlDataSource>

        </div>
    </form>
</body>
</html>
